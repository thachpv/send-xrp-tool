const { RippleAPI } = require('ripple-lib');
const BigNumber = require('bignumber.js');
const RIPPLE_SERVER = {
    MainNet: "wss://s1.ripple.com",
    TestNet: "wss://s.altnet.rippletest.net:51233",
    DevNest: "wss://s.devnet.rippletest.net:51233",
}

//========= DEVTEST ACCOUNT
// https://s.devnet.rippletest.net:51234

// rs6DaT6tiqXTabCiKtWBkQwtp22kgJox5y
// secret: sh6GmC4vS9TsQvi99Shiuw1fxYD3N

// receive: r3udw7AmF9KCsCxrFtRu8z3peRjLHa1yAq
// ================

/// ============== CUSTOM HERE ===========
const YOUR_DATA = {
    SEND_ADDRESS: "r3udw7AmF9KCsCxrFtRu8z3peRjLHa1yAq",
    SEND_SECRET: "sscd55KTsofhaTWdt17B5U5YpVFWf",
    DESTINATION_ADDRESS: "rGudQ5ifyzRpqEmYfi67MJTS9SDEfoiPcc",
    XRP_AMOUNT: "12",
    MEMO: 123, // Destination tag, là số nhé, không dùng thì cứ để vậy
}
// const CURRENT_ENV = RIPPLE_SERVER.DevNest;
// const CURRENT_ENV = RIPPLE_SERVER.TestNet;
const CURRENT_ENV = RIPPLE_SERVER.MainNet;
// =======================================


(async () => {
    const rippleConstants = {
        SUBMIT_TRANSACTION_SUCCESS_CODE: 'tesSUCCESS',
        TX_PENDING: 'tx_pending',
        TX_SUCCESS: 'tx_success',
        TX_FAILED: 'tx_failed',
        RESERVE_BASE_XRP_DEFAULT: 20,
        ACCOUNT_NOT_FOUND_ERROR_CODE: 19,
        PENDING_TX_RESULT_PREFIX: 'ter',
        SUCCESS_TX_RESULT_PREFIX: 'tes',
    };
    // wss://s.altnet.rippletest.net:51233
    const rippleAPI = new RippleAPI({ server: CURRENT_ENV });
    rippleAPI.on('error', (errorCode, errorMessage) => {
        console.log('Ripple connected: ', errorCode, errorMessage);
    });
    rippleAPI.on('connected', _ => {
        console.log('Ripple connected!');
    });
    await rippleAPI.connect();

    // get TX
    // let transaction = await rippleAPI.getTransaction(
    //     "5F46B85629E3FCEDE6CD40C71FDFCB4FB849E04C57856B1A209E52A3ADB66B92"
    // );
    // console.log({transaction})

    const correctAmount = new BigNumber(YOUR_DATA.XRP_AMOUNT).toFixed(7);

    const transactionData = {
        source: {
            address: YOUR_DATA.SEND_ADDRESS,
            maxAmount: {
                value: String(correctAmount),
                currency: 'XRP',
            },
        },
        destination: {
            address: YOUR_DATA.DESTINATION_ADDRESS,
            amount: {
                value: String(correctAmount),
                currency: 'XRP',
            },
            tag: YOUR_DATA.MEMO,
        },
    };

    try {
        const paymentPrepared = await rippleAPI.preparePayment(
            YOUR_DATA.SEND_ADDRESS,
            transactionData,
            {}
        );
        const { signedTransaction, id } = rippleAPI.sign(
            paymentPrepared.txJSON,
            YOUR_DATA.SEND_SECRET,
        );
        const submitResult = await rippleAPI.submit(signedTransaction);
        if (
            submitResult &&
            submitResult.resultCode ===
            rippleConstants.SUBMIT_TRANSACTION_SUCCESS_CODE
        ) {
            console.log("Success! txHash: ", id);
        }else {
            console.log("OOP: ", submitResult);
        }

    } catch (error) {
        console.log('Ripple service sendTransaction: ', error);
    }
    process.exit(0);
})();
