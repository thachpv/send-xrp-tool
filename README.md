### STEP 1: Install dependencies

```bash
npm install
```



### STEP 2: CUSTOM CONFIG

```javascript
const YOUR_DATA = {
    SEND_ADDRESS: "rs6DaT6tiqXTabCiKtWBkQwtp22kgJox5y",
    SEND_SECRET: "sh6GmC4vS9TsQvi99Shiuw1fxYD3N",
    DESTINATION_ADDRESS: "r3udw7AmF9KCsCxrFtRu8z3peRjLHa1yAq",
    XRP_AMOUNT: "12",
    MEMO: 123, // Destination tag, là số nhé, không dùng thì cứ để im vậy cho khỏi lỗi
}
const CURRENT_ENV = RIPPLE_SERVER.DevNest;
// const CURRENT_ENV = RIPPLE_SERVER.TestNet;
// const CURRENT_ENV = RIPPLE_SERVER.MainNet;
```

### STEP 3: RUN COMMAND TO SEND XRP
```
npm start
```
